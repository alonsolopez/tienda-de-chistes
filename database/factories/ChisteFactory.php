<?php

namespace Database\Factories;

use App\Models\Chiste;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChisteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Chiste::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "titulo" => "chiste #" . $this->faker->numberBetween(1, 100),
            "chiste" => "¿qué le dijo " . $this->faker->name('male') . " a " . $this->faker->name('female') . "... pues " . $this->faker->paragraph(2),
            "autor" => "Autor x",
            "pseudonimo" => "Pseudonimo x",
            //"categorias" => "2,3,4",
            "user_id" => 1,
        ];
    }
}