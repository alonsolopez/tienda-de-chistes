<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Chiste;
use App\Models\Categoria;
use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\ChisteSeeder;
use Database\Seeders\CategoriaSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        $this->call(CategoriaSeeder::class);
        Chiste::factory(10)
            ->create()
            ->each(function ($chiste)
            {
                $chiste->categorias()->attach(Categoria::find(2)->id);
                $chiste->categorias()->attach(Categoria::find(4)->id);
            });
        $this->call(ChisteSeeder::class);
    }
}