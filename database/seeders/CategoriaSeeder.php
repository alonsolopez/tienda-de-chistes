<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //arreglo para crear categorías de chistes de http://www.holasoyramon.com/chistes/categorias/
        $categorias = [
            [
                "nombre" => "Chiste Acertijo",
                "descripcion" => "No son verdaderos chistes son cuentecillos que hay que tratar de adivinar."
            ],
            [
                "nombre" => "Chistes Absurdos",
                "descripcion" => "Éstos no tienen sentido pero sí gracia."
            ],
            [
                "nombre" => "Chistes Basados en Hechos Reales",
                "descripcion" => "Suelen historias graciosas que son difíciles de clasificar y siempre se afirma que son reales."
            ],
            [
                "nombre" => "Chistes Cortos",
                "descripcion" => "Estos chistes pueden ser de otras categorías, lo único que los identifica es que son cortos, de 2 ó 3 líneas com mucho."
            ],
            [
                "nombre" => "Chistes De 'No es lo Mismo'",
                "descripcion" => "Es un género poco frecuente pero algunos son muy ingeniosos."
            ],
            [
                "nombre" => "Chistes de a ver quien es más...",
                "descripcion" => "Se juntan varios y dicen a ver quien más lo que sea..."
            ],
            [
                "nombre" => "Chistes de Ancianos",
                "descripcion" => "Suelen historias graciosas que son difíciles de clasificar y siempre se afirma que son reales."
            ],
            [
                "nombre" => "Chistes de Animales",
                "descripcion" => "¿Quién no ha oído un chiste de loros, o ha estado en el zoológico riéndose con la hiena?"
            ],
            [
                "nombre" => "Chistes de Bares",
                "descripcion" => "Son chistes de bares y discotecas en los que intervienen clientes y camareros."
            ],
        ];
        //recorrer arrglo y crear todas las catego
        foreach ($categorias as $catego)
        {
            //crear las categorias
            DB::table('categorias')->insert($catego);
        }
    }
}