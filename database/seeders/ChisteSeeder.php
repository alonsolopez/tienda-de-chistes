<?php

namespace Database\Seeders;

use App\Models\Chiste;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;

class ChisteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
        $chistes = [
            0 => [
                "titulo" => "chiste #100",
                "chiste" => "¿qué le dijo Kaleigh Gislason PhD a Miss Delfina Roob IV... pues Vel quam at placeat. Non praesentium dolores et autem velit. Qui a aut consequatur architecto eum vel.",
                "autor" => "Autor x",
                "pseudonimo" => "Pseudonimo x",
                //"categorias" => "2,3,4",
                "user_id" => 1,
            ],
            1 => [
                "titulo" => "chiste #101",
                "chiste" => "¿qué le dijo Mose Welch III a Vernie Rice Jr.... pues Consequatur dolore iste fugit et quaerat odio ullam. Est quibusdam eos laboriosam. Velit accusamus ut ipsum ut.",
                "autor" => "Autor x",
                "pseudonimo" => "Pseudonimo x",
                //"categorias" => "2,3,4",
                "user_id" => 1,
            ],
            2 => [
                "titulo" => "chiste #102",
                "chiste" => "¿qué le dijo Walton Weimann a Aaliyah Heathcote... pues Voluptatem est minima reiciendis nihil eius sunt earum facilis. Consequatur repellat ea est quas cum commodi aut. Quod numquam asperiores sunt natus exercitationem officia voluptatem.",
                "autor" => "Autor x",
                "pseudonimo" => "Pseudonimo x",
                //"categorias" => "2,3,4",
                "user_id" => 1,
            ],
        ];
        foreach ($chistes as $chiste)
        {
            # el chiste..
            $chisteNuevo = Chiste::create($chiste);
            // $idChiste = DB::table('chistes')->insert($chiste)->id;
            // sus catego
            $chisteNuevo->categorias()->attach(["categoria_id" => 2]);
            $chisteNuevo->categorias()->attach(["categoria_id" => 3]);
        }
    }
}