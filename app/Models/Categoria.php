<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    //las categorias a las que pertenece
    public function chistes()
    {
        return $this->belongsToMany(Chistes::class, 'categorias_chistes')->withPivot('chiste_id');
    }
}
