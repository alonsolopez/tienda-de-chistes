<?php

namespace App\Models;

use App\Models\User;
use App\Models\Categoria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Te7aHoudini\LaravelTrix\Traits\HasTrixRichText;

class Chiste extends Model
{
    use HasFactory;
    use HasTrixRichText;

    protected $fillable = [
        'titulo',
        'chiste',
        'autor',
        'pseudonimo',
        'user_id',
    ];

    //las categorias a las que pertenece
    public function categorias()
    {
        return $this->belongsToMany(Categoria::class, 'categorias_chistes')->withPivot('categoria_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}