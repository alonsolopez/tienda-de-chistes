<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoriaChiste extends Pivot
{
    use HasFactory;
    protected $table = "categorias_chistes";

    protected $fillable = [
        "categoria_id",
        "chiste_id",
    ];
}