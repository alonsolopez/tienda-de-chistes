<?php

namespace App\Http\Livewire;

use App\Models\Chiste;
use Livewire\Component;

class ChistesIndex extends Component
{
    public $header = "NADAAAA";
    public $chistes = [];
    public $seMuestraForm = false;

    public function render()
    {
        $this->chistes = Chiste::all();
        return view('livewire.chistes-index');
    }

    public function mount()
    {
        $this->chistes = Chiste::all();
    }

    public function hydrated()
    {
        //$this->chistes = Chiste::all();
    }
}