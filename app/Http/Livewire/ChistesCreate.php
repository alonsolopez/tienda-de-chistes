<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Chiste;
use Livewire\Component;
use App\Models\Categoria;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;

class ChistesCreate extends Component
{
    public $seMuestraForm = false;
    public $titulo = "";
    public $chiste = "";
    public $autor = "";
    public $pseudonimo = "";
    public $categorias = [];
    public $user_id = "";
    //las vars para los checkboxes
    public $categoriasShow = [];
    public $valorCheck = 0;
    //validaciones
    public function rules()
    {
        return [
            "titulo" => ['required', 'min:6', 'max:200'],
            "chiste" => ['required'], // Rule::unique('pages', 'slug')],
            "autor" => [],
            "pseudonimo" => [],
            "categorias" => [],
            "user_id" => ['required'],
        ];
    }
    protected $messages = [
        "titulo.required" => "Agregar título, ¿como vas a crear una page sin TíTULO?",
        "chiste.required" => "Este es el bueno...",
        "user_id.required" => "Es automático... con el usuario logeado",
    ];

    public function showForm()
    {
        $this->seMuestraForm = true;
    }

    public function render()
    {

        $this->loadCategorias();
        return view('livewire.chistes-create');
    }

    public function create()
    {
        //proceso "monolito"
        //guardar en modelo chiste
        // $guardado = Chiste::create($this->toModelUsAuth());
        // //vincular las categorias
        // //$categoriasIDs = explode(',', $this->categorias);
        // foreach ($this->categorias as $key => $catego)
        // {
        //     $guardado->categorias()->attach(['categoria_id' => $catego]);
        // }

        // proceso con API
        $response = Http::post('http://127.0.0.1:8001/api/chiste', $this->toModelUsAuth());
        // dd($this->toModelUsAuth());
        $response = $response->body();
        //dd($response);

        if (!$response) abort(400, 'Error al guardar el chiste nuevo...');
        //limpiar y mostrar form
        $this->resetForm();
    }

    public function usuarioQueRegistro()
    {
        return User::findOrFail($this->user_id)->name;
    }

    public function toModel()
    {
        return [
            "titulo" => $this->titulo,
            "chiste" => $this->chiste,
            "autor" => $this->autor,
            "pseudonimo" => $this->pseudonimo,
            "categorias" => $this->categorias,
            "user_id" => $this->user_id,
        ];
    }
    public function toModelUsAuth()
    {
        return [
            "titulo" => $this->titulo,
            "chiste" => $this->chiste,
            "autor" => $this->autor,
            "pseudonimo" => $this->pseudonimo,
            "categorias" => $this->categorias,
            "user_id" => auth()->user()->id,
        ];
    }

    public function resetForm()
    {
        $this->seMuestraForm = false;
        $this->titulo = "";
        $this->chiste = "";
        $this->autor = "";
        $this->pseudonimo = "";
        $this->categorias = [];
        $this->user_id = "";
    }

    public function loadCategorias()
    {
        $this->categoriasShow = Categoria::all();
    }

    public function selectCategory($id, $checked)
    {
        //dd($checked);
        // $this->categorias[] = count($this->categorias) > 0 ? "," . $id : $id;
        //verificamos si ya estaba agregado
        if ($checked && !in_array($id, $this->categorias)) //check ... no check
            $this->categorias[] =  $id;
        else unset($this->categorias[array_search($id, $this->categorias)]);
        // else if (!$checked && in_array($id, $this->categorias))
        //     unset($this->categorias[array_search($id, $this->categorias)]);
    }
}