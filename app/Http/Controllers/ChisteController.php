<?php

namespace App\Http\Controllers;

use App\Models\Chiste;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class ChisteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "chistes" => Chiste::all(),
            "footer" => "Foot",
            "tittle" => "Chistes INDEX",
        ];
        //muestra la vista index de chistes
        return view('chistes.index', compact('data'));
    }
    public function indexApi()
    {
        $data = [
            "chistes" => Chiste::all(),
            "footer" => "Foot",
            "tittle" => "Chistes INDEX",
        ];

        //muestra la vista index de chistes
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("chistes.create");
    }

    /**
     * Store a newly created resource in storage.
     * ---.API-----
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd('En el store por API---', $request);
        //validaciones para los campos
        $validacion = Validator::make($request->all(), [
            "titulo" => "required|min:6",
            "chiste" => "required",
            "autor" => "min:2",
            "pseudonimo" => "min:2",
        ]);
        if ($validacion->fails())
        {
            //dd("falló...", $request['titulo'], $validacion->messages());
            $mensajes = $validacion->messages();
            return response()->json("error en valicación de datos: '$mensajes'", 401);
        }
        else
        {
            //dd('en controller, no falló validacion... ', $request['categorias']);
            // explotamos categorias
            $categorias = isset($request['categorias']) && is_array($request['categorias']) ? $request['categorias'] : explode(',', $request['categorias']);
            //[3,15,16,1]
            //crearlo
            $guardado = Chiste::create($request->all());
            foreach ($categorias as $key => $categoria)
            {
                # iteramos sobre las categorias para añadirlas
                $guardado->categorias()->attach(['categoria_id' => $categoria]);
            }
            dd($guardado, $categorias);
        }
        //dd($request);
        //guardar el Chiste
        // $validados = $request->validate([
        //     "titulo" => "required|min:6",
        //     "chiste" => "required",
        //     "autor" => "min:6",
        //     "pseudonimo" => "min:6",
        // ]);

        //response API
        // dd($request, $guardado);
        return response()->json($guardado, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chiste  $chiste
     * @return \Illuminate\Http\Response
     */
    public function show(Chiste $chiste)
    {

        return view("chistes.show", compact('chiste'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chiste  $chiste
     * @return \Illuminate\Http\Response
     */
    public function edit(Chiste $chiste)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Chiste  $chiste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chiste $chiste)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chiste  $chiste
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chiste $chiste)
    {

        //obtener recurso
        //$chisteABorrar = Chiste::findOrFail($chiste->id);
        if ($chiste)
        {
            $chiste->delete();
            return response()->json("Chiste borrado.", 200);
        }
        else
            return response()->json("Chiste no existe. no se puede borrar.", 404);
    }
}