
<div class="p-6">

        <x-jet-button class="shadow-outline-purple hover:bg-green-300 py-2 px-3 ml-4 mt-4 px-6 text-lg text-purple-800 bg-green-500 inline-block rounded" wire:click="showForm">
            {{ __('+') }}
        </x-jet-button>


    {{-- Modal form --}}
        <x-jet-dialog-modal wire:model="seMuestraForm">
            <x-slot name="title">
                {{ __('Registra un Chiste Nuevo') }}
            </x-slot>

            <x-slot name="content">
                <div class="mt-4">
                    <x-jet-label for="title" value="{{ __('Título') }}" />
                    <x-jet-input id="title" class="block mt-1 w-full" type="text" wire:model.debounce.800ms="titulo"   />
                    @error('titulo') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label for="chiste" value="{{__('Chiste') }}" />
                    <div class="body-content px-2" wire:ignore >
                    {{-- <div class="body-content px-2" wire:ignore > --}}
                        <trix-editor class="trix-content" x-ref="trix" wire:model.debounce.100000ms="chiste" wire:key="trix-content-unique-key">

                        </trix-editor>
                        {{-- @trix(\App\Models\Chiste::class, wire:model="chiste", ['hideTools'=>['file-tools']]) --}}
                        {{-- @trix(\App\Models\Chiste::class, 'chiste', ['hideTools'=>['file-tools']]) --}}
                    </div>
                    @error('chiste') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label for="autor" value="{{ __('Autor') }}" />
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <x-jet-input id="autor" class="form-input flex-1 block  w-full rounded-none rounded-r-md transition duration-150 ease-in-out sm:text-sm sm:leading-5 " type="text" wire:model="autor" placeholder="Quien es el autor del chiste"/>
                    </div>
                    @error('autor') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label for="pseudonimo" value="{{ __('Pseudonimo') }}" />
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <x-jet-input id="pseudonimo" class="form-input flex-1 block  w-full rounded-none rounded-r-md transition duration-150 ease-in-out sm:text-sm sm:leading-5 " type="text" wire:model="pseudonimo" placeholder="El pseudónimo "/>
                    </div>
                    @error('pseudonimo') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label  value="{{ __('Categorías') }}" />
                    <div class="mt-1 flex rounded-md shadow-sm text-xs overflow-auto px-1">

                        @for ($i = 0; $i < count($categoriasShow) ; $i++)
                            <div class="border border-purple-400 rounded bg-purple-300 px-1 mx-3">
                                <input type="checkbox" id="{{$i}}" name="{{$i}}" wire:click="selectCategory({{$categoriasShow[$i]['id']}}, this)"   ><span class="text-xs text-purple-700">{{$categoriasShow[$i]['nombre']}}</span>
                            </div>
                        @endfor

                        {{-- @foreach ($categorias as $categoria)
                        <div class="border border-purple-400 rounded bg-purple-300 px-1 mx-3">
                            <input type="checkbox" id="{{$categoria->id}}" wire:model="catetorias[$categoria->id]" ><span class="text-xs text-purple-700">{{$categoria->nombre}}</span>
                        </div>
                        @endforeach --}}
                    </div>
                    @error('categorias') <span class="error">{{$message}}</span> @enderror
                </div>
                {{-- <div class="mt-4">
                    <x-jet-label for="usuario" value="{{ __('Usuario') }}" />
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <x-jet-input id="usuario" class="form-input flex-1 block  w-full rounded-none rounded-r-md transition duration-150 ease-in-out sm:text-sm sm:leading-5 " type="text" placeholder="ID de usuario"/>
                    </div>
                    @error('user_id') <span class="error">{{$message}}</span> @enderror
                </div> --}}


                {{-- un control de password --}}
                {{-- <div class="mt-4" x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)">
                    <x-jet-input type="password" class="mt-1 block w-3/4" placeholder="{{ __('Password') }}"
                                x-ref="password"
                                wire:model.defer="password"
                                wire:keydown.enter="deleteUser" />

                    <x-jet-input-error for="password" class="mt-2" />
                </div> --}}
            </x-slot>
            {{print_r($valorCheck)}}
            <x-slot name="footer">
                <x-jet-secondary-button wire:click="$toggle('seMuestraForm')" wire:loading.attr="disabled">
                    {{ __('Olvídalo...') }}
                </x-jet-secondary-button>

                <x-jet-button class="ml-2 bg-green-500" wire:click="create" wire:loading.attr="disabled">
                    {{ __('Guardar Página') }}
                </x-jet-button>
                {{print_r($categorias)}}
            </x-slot>
        </x-jet-dialog-modal>
</div>
