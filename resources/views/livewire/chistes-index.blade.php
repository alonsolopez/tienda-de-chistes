<div class="container max-w-full">
   <hr>

<div class="mx-4 sm:-mx-8 px-4 sm:px-8  overflow-x-auto">
    <div class="inline-block max-w-full shadow rounded-lg overflow-hidden">
        <table class="max-w-full leading-normal">
            <thead>
                <tr>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                        Título
                    </th>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                        Chiste
                    </th>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                        Autor
                    </th>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                        Pseudónimo
                    </th>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                        Usuario que registró
                    </th>
                    <th
                        class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-right text-xs font-semibold text-purple-600 uppercase tracking-wider">
                        Categorías
                    </th>
                </tr>
            </thead>
            <tbody>

                @foreach ($chistes as $chiste)
                <tr>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <a href="/chistes/{{$chiste['id']}}" class="shadow-inner hover:text-2xl hover:text-purple-500  hover:underline" >
                            <p class="text-gray-900 whitespace-no-wrap">{{$chiste['titulo']}}</p>
                        </a>
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="my-3 text-grey font-light tracking-wide font-sans leading-normal text-sm">{!! $chiste->chiste !!}</p>
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 whitespace-no-wrap">{{$chiste->autor}}</p>
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 whitespace-no-wrap">{{$chiste->pseudonimo}}</p>
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 whitespace-no-wrap">{{$chiste->user->name}}</p>
                    </td>
                    <td class="w-max-content text-right px-2 py-1 border-b border-gray-200  text-xs">
                        <ul class=" list-reset flex flex-col bg-purple-100 ">
                            @foreach ($chiste->categorias as $catego)
                                <li class="relative -mb-px block border border-grey">{{$catego->nombre}}</li>
                            @endforeach
                            {{-- <li class=" rounded-t relative -mb-px block border border-grey">Cras justo odio</li>
                            <li class="relative -mb-px block border border-grey">Morbi leo risus</li>
                            <li class="relative -mb-px block border border-grey">Porta ac consectetur ac</li>
                            <li class="rounded-b relative block border border-grey">Vestibulum at eros</li> --}}
                        </ul>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>


<hr>
</div>
