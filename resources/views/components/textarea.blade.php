<div>
    <textarea
         {{ $attributes->except('class') }}
         {{ $attributes->merge(['class' => 'w-full']) }}
    ></textarea>

     @error($attributes['wire:model'])
       {{-- <x-validation-error> --}}
         {{ $message }}
       {{-- </x-validation-error> --}}
     @enderror
 </div>
