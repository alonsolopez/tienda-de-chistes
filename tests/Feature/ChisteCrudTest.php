<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Chiste;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\CategoriaSeeder;

class ChisteCrudTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_un_chiste_se_puede_crear()
    {
        //msg descriptivo de errores
        $this->withoutExceptionHandling();
        //arrange y act
        User::factory(5)->create();
        //corre los seeders agregados
        $this->seed();
        // $usuario = User::find(1);
        // dd($usuario);
        $response = $this->post('http://127.0.0.1:8001/api/chiste', [
            "titulo" => "Un chiste de prueba",
            "chiste" => "el cuerpo del chiste de prueba",
            "autor" => "alonso prueba",
            "pseudonimo" => "alonso prueba",
            "categorias" => [
                0 => 4,
                1 => 5,
                2 => 6,
            ],
            "user_id" => 1,
            // "user_id" => $usuario->id,
            // "user_id" => auth()->user()->id,
        ]);

        //$response = $chiste->categorias()->attach(["categoria_id" => 1]);

        //assert
        $response->assertOk();
        //$this->assertCount(1, Chiste::all());
        // $response->assertStatus(200);
    }
}